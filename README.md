「簡素化されたクリエイティブ作品に対するライセンス」について
============================================================

# 1. これは何ですか？

  * [MIT-license](http://opensource.org/licenses/MIT) 
     * 正確には、[その邦訳](http://sourceforge.jp/projects/opensource/wiki/licenses%2FMIT_license)

を元にして作った、

  * 創作物 (小説や、イラスト、音楽等)

に対して利用する事を目的とした、シンプルなライセンスです。

# 2. 特徴は？

このライセンスの特徴としては、

  1. 作品の利用には、 *作品の著作権表示* と、 *ライセンス表示* を行うコトが必要となるコト
  2. 作品、またはその派生物の利用の際、 *明らかな商業的活動* を行う場合には、原著作権者の明確な許諾がいるコト

の二点です。

# 3. なぜこのライセンスを作ったのですか？

理由としては、

  1. 自身の作った創作物(小説等)について、簡素なライセンスが欲しかった
  2. [同人マーク](http://commonsphere.jp/doujin)や、[CreativeCommons](https://creativecommons.org/)が、微妙に合わなかった
  3. 創作物に対する[MIT-license](http://opensource.org/licenses/MIT)相当のライセンスが欲しかった

という辺りです。

# 4. このライセンスは自由に使えますか？

ライセンス文に対しては、確か著作権が発生しなかったはず、だと思うのですが、
ここでは明示的に、本ライセンス表示を、

  * [CC0 (public domain)](https://creativecommons.org/publicdomain/zero/1.0/)

とします。なので、真の意味で自由に使ってもかまいません。

しかしながら、本ライセンス文の中身については、一切の保証は行いませんので、
利用する際におきましては、各自の責任の元に利用をお願いします。

# 5. 作成者について

  * 岡村 直樹 (にゃるら) <nyarla@thotep.net> [@nyarla](https://twitter.com/nyarla)

---

問題点の指摘等ありましたら、

  * [Issues · nyarla/simplified-creative-license](https://github.com/nyarla/simplified-creative-license/issues)

までお願いします。

# 6. 更新履歴

  * 2014-03-19
     * 全面的に作り直した
  * 2014-01-18
     * 初版

